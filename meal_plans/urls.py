from django.urls import path
from meal_plans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanDetailView,
    MealPlanUpdateView,
)

urlpatterns = [
    path("list/", MealPlanListView.as_view(), name="meal_plans_list"),
    path(
        "meal_plans/create/",
        MealPlanCreateView.as_view(),
        name="meal_plan_create",
    ),
    path(
        "<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_plan_edit",
    ),
]

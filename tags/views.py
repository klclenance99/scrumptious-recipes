from msilib.schema import ListView
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


from tags.models import Tag


# Create your views here.


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"


class TagCreateView(CreateView):
    model = Tag
    template_name = "tags/create.html"
    success_url = reverse_lazy("tags_list")


class TagUpdateView(UpdateView):
    model = Tag
    template_name = "tags/edit.html"
    success_url = reverse_lazy("tags_list")


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")

from django.urls import path

from tags.views import (
    TagCreateView,
    TagDeleteView,
    TagDetailView,
    TagListView,
    TagUpdateView,
)

urlpatterns = [
    path("", TagListView.as_view(), name="tags_list"),
    path("create/", TagCreateView.as_view(), name="tag_create"),
    path("<int:pk>/edit/", TagUpdateView.as_view(), name="tag_edit"),
    path("<int:pk>/detail/", TagDetailView.as_view(), name="tag_detail"),
    path("<int:pk>/delete", TagDeleteView.as_view(), name="tag_delete"),
]

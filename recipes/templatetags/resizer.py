from logging import exception
from django import template

register = template.Library()


def resize_to(ingredient, target):
    serv = ingredient.recipe.serving_size
    print(target)
    print(serv)
    if serv is not None and target is not None:
        try:
            ratio = int(target) / serv
            new_amount = ratio * ingredient.amount
            return new_amount
        except:
            pass
    return ingredient.amount


register.filter(resize_to)

from django.contrib import admin
from recipes.models import Recipe
from recipes.models import FoodItem
from recipes.models import Measure
from recipes.models import Ingredient
from recipes.models import Step
from recipes.models import Rating

# Register your models here.
class RecipeAdmin(admin.ModelAdmin):
    pass


admin.site.register(Recipe, RecipeAdmin)


class FoodItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(FoodItem, FoodItemAdmin)


class MeasureAdmin(admin.ModelAdmin):
    pass


admin.site.register(Measure, MeasureAdmin)


class IngredientAdmin(admin.ModelAdmin):
    pass


admin.site.register(Ingredient, IngredientAdmin)


class StepAdmin(admin.ModelAdmin):
    pass


admin.site.register(Step, StepAdmin)


class RatingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Rating, RatingAdmin)
